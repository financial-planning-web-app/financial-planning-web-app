"""View functions.
These functions render pages for the user.
"""

from application import app, db
from application.forms import LoginForm, RegistrationForm, EditProfileForm, EditEmailForm, \
    EditPasswordForm, CreateThreadForm, EditThreadForm, ReplyThreadForm, EditPostForm, SectionForm
from application.models import Authority, User, Post, Section, Thread, EditInfo
from application.forms import ResetPasswordRequestForm, ResetPasswordForm, ContactForm
from application.email import send_password_reset_email
from flask import render_template, flash, redirect, url_for, request, abort, send_file
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from flask_mail import Message, Mail
from datetime import datetime, timedelta
from sqlalchemy import desc, func
from sqlalchemy.sql import label
import math


########## Navigation Bar ##########

@app.route("/")
@app.route("/home")
def index():
  return render_template("index.html", title='Home')  #template folder

@app.route("/concepts")
def concepts():
  return render_template("concepts.html", title='Important Concepts')


# @app.route("/overview")
# def overview():
#     return render_template("overview.html", title='Overview')


@app.route("/calendarevents")
def calendarevents():
  return render_template("calendarevents.html", title='Events')


# @app.route("/contact")
# def contact():
#     return render_template("contact.html", title='Contact')

@app.route("/info")
def information():
  return render_template("info.html", title='Special Needs Planning')

@app.route("/resources")
def resources():
  return render_template("resources.html", title='Resources')


@app.route("/targetaudience")
def targetaudience():
  return render_template("targetaudience.html", title='Target Audience')


# @app.route("/financialplanning")
# def financialplanning():
#     return render_template("financialplanning.html", title='Financial Planning')

# @app.route("/legal")
# def legal():
#     return render_template("legal.html", title='legal')

# @app.route("/estateplanning")
# def estateplanning():
#     return render_template("estateplanning.html", title='Estate Planning')


@app.route("/calendar")
def calendar():
  return render_template("calendar.html", title='Calendar')


@app.route("/searchresult")
def searchresult():
  return render_template("searchresult.html", title='Search Results')

########## User ##########
"""View functions related to the user.
"""

@app.before_request
def before_request():
  """A function that automatically executes right before each view function.
  Currently, it just updates the last seen time for users.
  """
  # if logged in
  if current_user.is_authenticated:
    # update user's last_seen time
    current_user.last_seen = datetime.utcnow()
    # user is invoked from and already in database session, so just commit
    db.session.commit()

# Note the get and post are necessary if extracting or sending information to a form
@app.route('/login', methods=['GET', 'POST'])
def login():
  """The view function for the login page.
  """
  # logged in already?
  if current_user.is_authenticated:
    return redirect(url_for('index'))
  form = LoginForm()
  # the browser sends POST request when the user click the submit button
  # validate_on_submit() returns True when it's POST
  # and all validators of the form have no complain
  if form.validate_on_submit():
    user = User.query.filter_by(username=form.username.data).first()
    if user is None:
      flash('Invalid username')
      return redirect(url_for('login'))
    elif not user.check_password(form.password.data):
      flash('Incorrect password')
      return redirect(url_for('login'))
    elif user.is_banned():
      flash('This account is banned')
      return redirect(url_for('login'))
    login_user(user, remember=form.remember_me.data)
    # go back to previous page after login (Example: .../login?next=/home)
    next_page = request.args.get('next')
    # if no next_page or URL is directing another website, go to index
    # check whether URL is local to protect from attackers
    if not next_page or url_parse(next_page).netloc != '':
      next_page = url_for('index')
    return redirect(next_page)
  return render_template('user/login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
  """Function to logout.
  It does not render a page.
  """
  logout_user()
  return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
  """The view function for the register page.
  """
  if current_user.is_authenticated:
    return redirect(url_for('index'))
  form = RegistrationForm()
  if form.validate_on_submit():
    user = User(username=form.username.data, displayname=form.displayname.data,
        email=form.email.data)
    user.set_password(form.password.data)
    db.session.add(user)
    db.session.commit()
    flash('Congratulations, you are now a registered user!')
    return redirect(url_for('login'))
  return render_template('user/register.html', title='Register', form=form)


@app.route('/user/<username>', methods=['GET', 'POST'])
@login_required
def user(username):
  """The view function for the user profile page.
  Args:
    username: A string of the user's username.
  Raises:
    403: When unauthorized users try to access super-user authorities.
  """
  user = User.query.filter_by(username=username).first_or_404()
  recent_activities = user.posts.order_by(desc(Post.timestamp)).limit(5).all()
  action = request.args.get('action')

  if action == 'admin':
    # make sure the giver has the right
    if current_user.get_authority() != Authority.webmaster:
      abort(403)
    if user.get_authority() == Authority.admin:
      user.set_authority(Authority.user)
    else:
      user.set_authority(Authority.admin)
    db.session.commit()
    return redirect(url_for('user', username = user.username))
  elif action == 'ban':
    if not current_user.is_superuser():
      abort(403)
    if user.get_authority() == Authority.banned:
      user.set_authority(Authority.user)
    else:
      user.set_authority(Authority.banned)
    db.session.commit()
  return render_template('user/user.html', title=username + '\'s Profile', user=user,
      recent_activities=recent_activities, datetime=datetime)


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
  """The view function for the edit profile page.
  """
  form = EditProfileForm(current_user.displayname)
  if form.validate_on_submit():
    current_user.displayname = form.displayname.data
    current_user.about_me = form.about_me.data
    db.session.commit()
    flash('Your changes have been saved.')
    return redirect(url_for('user', username = current_user.username))
  # initial version
  elif request.method == 'GET':
    form.displayname.data = current_user.displayname
    form.about_me.data = current_user.about_me
  return render_template('user/edit_profile.html', title='Edit Profile', form=form)


@app.route('/settings')
@app.route('/settings/<field>', methods=['GET', 'POST'])
@login_required
def settings(field=None):
  """The view function for the settings page.
  Args:
    field: Must be 'email', 'password' or None. Indicates which field to edit. None means main page.
  Raises:
    404: Invalid field.
  """
  form = None
  if field == 'email':
    form = EditEmailForm(current_user.email)
    if form.validate_on_submit():
      current_user.email = form.email.data
      db.session.commit()
      flash('Your email has been changed.')
      return redirect(url_for('settings'))
  elif field == 'password':
    form = EditPasswordForm(current_user)
    if form.validate_on_submit():
      current_user.set_password(form.password.data)
      db.session.commit()
      flash('Your password has been changed.')
      return redirect(url_for('settings'))
  elif field != None:
    abort(404)
  return render_template('user/settings.html', title='Settings', field=field, form=form,
      user=current_user)


########## forum ##########
"""View functions related to the forum.
Forum Structure:
  - forum main page
    - sections
      - threads
        - topic post
        - reply posts
"""

# forum main page & forum section pages
@app.route('/forum')
@app.route('/forum/<id>', methods=['GET', 'POST'])
@app.route('/forum/<id>/<page>', methods=['GET', 'POST'])
@login_required
def forum(id=None, page=1):
  """The view function for the main forum page and the forum section pages.
  Args:
    id: Optional. Forum section id or None. This function renders the main forum page if it's None
    page: Optional. Page of the section. Only useful when id is not None.
  Raises:
    403: When unauthorized users try to access super-user authorities.
  """
  # main forum page, showing all sections
  if id is None:
    sections = Section.query.filter_by(closed=False).all()
    closed_sections = Section.query.filter_by(closed=True).all()
    total_users = User.query.count()
    new_user = User.query.order_by(desc(User.member_since)).first()
    # get top 3 most replied threads in past 7 days
    hot_threads_obj = db.session.query(
      Post.thread_id, label('replies', func.count(Post.id))).filter(
        Post.timestamp > (datetime.utcnow()-timedelta(days=3))).group_by(
          Post.thread_id).order_by(desc('replies')).all()[0:3]
    hot_threads = []
    for obj in hot_threads_obj:
      hot_threads.append(Thread.query.get(obj.thread_id))
    return render_template('forum/forum.html', title='Forum', route='forum',
        closed_sections=closed_sections, sections=sections, datetime=datetime,
        total_users=total_users, new_user=new_user, hot_threads=hot_threads)

  # for a super user to edit this section
  form = SectionForm()
  show_form = False
  this_section = Section.query.filter_by(id=id).first_or_404()
  close_section_id = request.args.get('close_section_id')

  # admin closes/uncloses this section
  if close_section_id and close_section_id == id:
    if not current_user.is_superuser():
      abort(403)
    if this_section.closed:
      this_section.closed = False
    else:
      this_section.closed = True
    db.session.commit()
    return redirect(url_for('forum', id=id, page=page))

  if form.validate_on_submit():
    # a non-super user shouldn't be able to see the editing form
    if not current_user.is_superuser():
      abort(403)
    this_section.name = form.name.data
    this_section.slogan = form.slogan.data
    this_section.description = form.description.data
    db.session.commit()
  elif form.submit.data:
    # show form when validators didn't pass after clicking submit button
    show_form = True
  else:
    # set the initial version for the editing form
    form.name.data = this_section.name
    form.slogan.data = this_section.slogan
    form.description.data = this_section.description

  # page of a forum section, showing all threads of this section and this page
  last_page = math.ceil(this_section.threads.count() / app.config['MAX_THREADS'])
  this_page = int(page)
  page_begin=max(1, this_page-app.config['NEIGHBOR_PAGES'])
  page_end=min(last_page, this_page+app.config['NEIGHBOR_PAGES'])
  threads = this_section.threads.order_by(desc(Thread.pinned), desc(Thread.id)).paginate(
      page=this_page, per_page=app.config['MAX_POSTS'])
  return render_template('forum/forum_section.html', title=this_section.name, threads=threads.items,
      this_section=this_section, form=form, show_form=show_form, datetime=datetime,
      # parameters for the page selection:
      route='forum', id=id, this_page=this_page, last_page=last_page, page_begin=page_begin,
      page_end=page_end)


@app.route('/create_thread/<id>', methods=['GET', 'POST'])
@login_required
def create_thread(id):
  """The view function for the create thread page.
  Args:
    id: Forum section id. The section you want to add a new thread to.
  Raises:
    403: When unauthorized users try to access super-user authorities.
  """
  form = CreateThreadForm()
  this_section = Section.query.filter_by(id=id).first_or_404()

  if this_section.closed and not current_user.is_superuser():
    abort(403)

  if form.validate_on_submit():
    new_thread = Thread(topic=form.topic.data, section=this_section)
    db.session.add(new_thread)
    new_post = Post(body=form.body.data, author=current_user, thread=new_thread)
    db.session.add(new_post)
    db.session.commit()
    flash('Your created a new thread.')
    return redirect(url_for('thread', id=new_thread.id))
  return render_template('forum/create_thread.html', title='Create a thread', form=form,
      section=this_section)


@app.route('/edit_thread/<id>', methods=['GET', 'POST'])
def edit_thread(id):
  """The view function for the edit thread page.
  Args:
    id: Thread id.
  Raises:
    403: When unauthorized users try to access super-user authorities.
  """
  this_thread = Thread.query.filter_by(id=id).first_or_404()
  this_topic_post = this_thread.posts.first_or_404()
  this_section = this_thread.section
  form = EditThreadForm()

  # a normal user shouldn't edit other users' posts
  if current_user != this_topic_post.author and not current_user.is_superuser():
    abort(403)

  if form.validate_on_submit():
    this_thread.topic = form.topic.data
    this_topic_post.body = form.body.data
    if this_topic_post.edit_info:
      this_topic_post.edit_info.reason = form.reason.data
      this_topic_post.edit_info.timestamp = datetime.utcnow()
      this_topic_post.edit_info.editor = current_user
    else:
      # first time editing of this post
      edit_info = EditInfo(reason=form.reason.data, post=this_topic_post, editor=current_user)
      db.session.add(edit_info)
    db.session.commit()
    return redirect(url_for('thread', id=id))
  else:
    # the initial version
    form.topic.data = this_thread.topic
    form.body.data = this_topic_post.body

  return render_template('forum/create_thread.html', title='Edit a thread', form=form,
      section=this_section)


@app.route('/thread/<id>', methods=['GET', 'POST'])
@app.route('/thread/<id>/<page>', methods=['GET', 'POST'])
@app.route('/thread/<id>/<page>/<edit_post_id>', methods=['GET', 'POST'])
@login_required
def thread(id, page=1, edit_post_id=None):
  """The view function for a thread page.
  Users can reply to this thread and edit reply posts within this page.
  Args:
    id: Thread id.
    page: Page of this thread to render.
    edit_post_id: The id of the post to edit.
  Raises:
    403: When unauthorized users try to access super-user authorities.
    404: Invalid edit_post_id when submitting edit post form
  """
  this_thread = Thread.query.filter_by(id=id).first_or_404()
  reply_form = ReplyThreadForm()
  edit_post_form = EditPostForm()
  scroll_to = request.args.get('scroll_to')
  close_post_id = request.args.get('close_post_id', type=int)
  action = request.args.get('action')

  # regular users cannot access a thread if its section is closed
  if this_thread.section.closed and not current_user.is_superuser():
    return redirect(url_for('forum', id=this_thread.section.id))

  # count thread views
  if current_user not in this_thread.viewers:
    this_thread.viewers.append(current_user)
    db.session.commit()

  # admin closes/uncloses a reply post
  if close_post_id:
    if not current_user.is_superuser():
      abort(403)
    post = Post.query.filter_by(id=close_post_id).first_or_404()
    if post.closed:
      post.closed = False
    else:
      post.closed = True
    db.session.commit()
    return redirect(url_for('thread', id=id, page=page, scroll_to=post.id))

  # admin closes/uncloses a thread
  if action == 'close':
    if not current_user.is_superuser():
      abort(403)
    thread = Thread.query.filter_by(id=id).first_or_404()
    topic_post = thread.topic_post()
    if thread.closed:
      thread.closed = False
      topic_post.closed = False
    else:
      thread.closed = True
      topic_post.closed = True
    db.session.commit()
    return redirect(url_for('thread', id=id))
  elif action == 'pin':
    if not current_user.is_superuser():
      abort(403)
    thread = Thread.query.filter_by(id=id).first_or_404()
    if thread.pinned:
      thread.pinned = False
    else:
      thread.pinned = True
    db.session.commit()
    return redirect(url_for('thread', id=id))

  # user clicks the submit button of the reply form
  if reply_form.submit_reply.data and reply_form.validate_on_submit():
    new_reply_post = Post(body=reply_form.body.data, author=current_user, thread=this_thread)
    db.session.add(new_reply_post)
    db.session.commit()
    last_page = math.ceil(this_thread.posts.count() / app.config['MAX_POSTS'])
    return redirect(url_for('thread', id=id, page=last_page, scroll_to=new_reply_post.id))

  # uer edits a post
  elif edit_post_form.submit_save.data and edit_post_form.validate_on_submit():
    # edit_post_id should be passed by the "edit" button before user submit it
    if not edit_post_id:
      abort(404)
    post = Post.query.filter_by(id=edit_post_id).first_or_404()
    # a normal user shouldn't edit other users' posts
    if current_user != post.author and not current_user.is_superuser():
      abort(403)
    post.body = edit_post_form.body.data
    if post.edit_info:
      post.edit_info.reason = edit_post_form.reason.data
      post.edit_info.timestamp = datetime.utcnow()
      post.edit_info.editor = current_user
    else:
      # first time editing of this post
      edit_info = EditInfo(reason=edit_post_form.reason.data, post=post, editor=current_user)
      db.session.add(edit_info)
    db.session.commit()
    return redirect(url_for('thread', id=id, page=page, scroll_to=post.id))

  # clicked "Edit" button to start editing, set form to the initial version
  elif edit_post_id:
    post = Post.query.filter_by(id=edit_post_id).first_or_404()
    edit_post_form.body.data = post.body

  this_page = int(page)
  last_page = math.ceil(this_thread.posts.count() / app.config['MAX_POSTS'])
  posts = this_thread.posts.paginate(page=this_page, per_page=app.config['MAX_POSTS'])
  page_begin = max(1, this_page-app.config['NEIGHBOR_PAGES'])
  page_end = min(last_page, this_page+app.config['NEIGHBOR_PAGES'])

  # show post objects of this page
  return render_template('forum/thread.html', title=this_thread.topic, posts=posts.items,
      reply_form=reply_form, edit_post_form=edit_post_form, edit_post_id=edit_post_id, scroll_to=scroll_to,
      datetime=datetime, this_thread=this_thread,
      # parameters for the page selection:
      route='thread', id=id, this_page=this_page, last_page=last_page, page_begin=page_begin,
      page_end=page_end)


@app.route('/create_section', methods=['GET', 'POST'])
@login_required
def create_section():
  """The view function for the create forum section page.
  Raises:
    403: When unauthorized users try to access super-user authorities.
  """
  # a non-super user shouldn't be in this page
  if not current_user.is_superuser():
    abort(403)

  form = SectionForm()
  if form.validate_on_submit():
    new_section = Section(name=form.name.data, slogan=form.slogan.data,
        description=form.description.data)
    db.session.add(new_section)
    db.session.commit()
    flash('Your created a new section.')
    return redirect(url_for('forum', id=new_section.id))
  return render_template('forum/create_section.html', title='Create a section', form=form)


@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
  if current_user.is_authenticated:
    return redirect(url_for('index'))
  form = ResetPasswordRequestForm()
  if form.validate_on_submit():
    user = User.query.filter_by(email=form.email.data).first()
    if user:
      send_password_reset_email(user)
    flash('Check your email for the instructions to reset your password')
    return redirect(url_for('login'))
  return render_template('user/reset_password_request.html',
      title='Reset Password', form=form)


@app.route('/reset_user_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
  if current_user.is_authenticated:
    return redirect(url_for('index'))
  user = User.verify_reset_password_token(token)
  if not user:
    return redirect(url_for('index'))
  form = ResetPasswordForm()
  if form.validate_on_submit():
    user.set_password(form.password.data)
    db.session.commit()
    flash('Your password has been reset.')
    return redirect(url_for('login'))
  return render_template('user/reset_user_password.html', form=form)

mail = Mail()
@app.route('/contactUs', methods=['GET', 'POST'])
def contactUs():
  form = ContactForm()

  if request.method == 'POST':
    if form.validate() == False:
      flash('All fields are required.')
      return render_template('contactUs.html', form=form)
    else:
      msg = Message(form.subject.data, sender='contact@example.com', recipients=['Thoroughway123@gmail.com'])
      msg.body = """
      From: %s <%s>
      %s
      """ % (form.name.data, form.email.data, form.message.data)
      mail.send(msg)

      return render_template('contactUs.html', success=True)

  elif request.method == 'GET':
    return render_template('contactUs.html', form=form)

@app.route('/download/<filename>', methods=['GET', 'POST'])
def download(filename):
  path = "download/" + filename
  try:
    return send_file(path, as_attachment=True)
  except FileNotFoundError:
    abort(404)
