"""set nullable=False for some columns

Revision ID: cad639fa055c
Revises: 8de75ac6d7aa
Create Date: 2020-11-22 19:25:10.122047

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cad639fa055c'
down_revision = '8de75ac6d7aa'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('post', schema=None) as batch_op:
        batch_op.alter_column('body',
               existing_type=sa.VARCHAR(length=140),
               nullable=False)
        batch_op.alter_column('thread_id',
               existing_type=sa.INTEGER(),
               nullable=False)
        batch_op.alter_column('user_id',
               existing_type=sa.INTEGER(),
               nullable=False)

    with op.batch_alter_table('section', schema=None) as batch_op:
        batch_op.alter_column('name',
               existing_type=sa.VARCHAR(length=30),
               nullable=False)

    with op.batch_alter_table('thread', schema=None) as batch_op:
        batch_op.alter_column('section_id',
               existing_type=sa.INTEGER(),
               nullable=False)
        batch_op.alter_column('topic',
               existing_type=sa.VARCHAR(length=60),
               nullable=False)

    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('displayname',
               existing_type=sa.VARCHAR(length=30),
               nullable=False)
        batch_op.alter_column('email',
               existing_type=sa.VARCHAR(length=140),
               nullable=False)
        batch_op.alter_column('password_hash',
               existing_type=sa.VARCHAR(length=140),
               nullable=False)
        batch_op.alter_column('username',
               existing_type=sa.VARCHAR(length=30),
               nullable=False)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('username',
               existing_type=sa.VARCHAR(length=30),
               nullable=True)
        batch_op.alter_column('password_hash',
               existing_type=sa.VARCHAR(length=140),
               nullable=True)
        batch_op.alter_column('email',
               existing_type=sa.VARCHAR(length=140),
               nullable=True)
        batch_op.alter_column('displayname',
               existing_type=sa.VARCHAR(length=30),
               nullable=True)

    with op.batch_alter_table('thread', schema=None) as batch_op:
        batch_op.alter_column('topic',
               existing_type=sa.VARCHAR(length=60),
               nullable=True)
        batch_op.alter_column('section_id',
               existing_type=sa.INTEGER(),
               nullable=True)

    with op.batch_alter_table('section', schema=None) as batch_op:
        batch_op.alter_column('name',
               existing_type=sa.VARCHAR(length=30),
               nullable=True)

    with op.batch_alter_table('post', schema=None) as batch_op:
        batch_op.alter_column('user_id',
               existing_type=sa.INTEGER(),
               nullable=True)
        batch_op.alter_column('thread_id',
               existing_type=sa.INTEGER(),
               nullable=True)
        batch_op.alter_column('body',
               existing_type=sa.VARCHAR(length=140),
               nullable=True)

    # ### end Alembic commands ###
